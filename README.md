﻿# UltraViewPager

UltraViewPager是PageSlider的扩展，封装了多个功能，为多页面切换提供解决方案。

## 主要特征

 * 支持水平和垂直滚动
 * 支持循环切换视图
 * 支持自动滚动功能
 * 具有内置治时期，支持圆圈和图标样式

## 设计

UltraViewPager是PageSilder的扩展，它是一个DependentLayout以便显示PageSlider和indicator,
UltraViewPager为PageSlider提供了常见的方法委托，您可以通过调用getViewPager()来调用更多的方法，
并获取实际的PageSlider。

## 演示

|横向滑动 | 纵向滑动|
|:---:|:---:|
|<img src="https://gitee.com/openharmony-tpc/UltraViewPager/raw/master/screenshot/horzontal.gif" width="75%"/>|<img src="https://gitee.com/openharmony-tpc/UltraViewPager/raw/master/screenshot/vertical.gif" width="75%"/>|

|屏幕两边页面缩进 |切换页面缩放特效|
|:---:|:---:|
|暂不支持|暂不支持|

## entry运行要求
 通过DevEco studio,并下载openHarmonySDK
 将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）

## 集成

```
方式一：
通过library生成har包，添加har包到libs文件夹内
在entry的gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

方式二：
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:UltraViewPager:1.0.6'
```

## 使用
在layout中使用UltraViewPager：
ability_pager.xml
```xml
 <com.tmall.ultraviewpager.UltraViewPager
         ohos:id="$+id:ultra_viewpager"
         ohos:height="match_content"
         ohos:width="match_parent"
         ohos:background_element="#999999"
         ohos:center_in_parent="true"
         ohos:layout_alignment="center"
         />
```

可以参考以下步骤使用UltraViewPager:
```
 UltraViewPager ultraViewPager = (UltraViewPager) findComponentById(ResourceTable.Id_ultra_viewpager);
 ultraViewPager.setScrollMode(UltraViewPager.ScrollMode.HORIZONTAL);
 UltraPagerAdapter  adapter = new UltraPagerAdapter(PagerSlice.this, false);
 ultraViewPager.setProvider(adapter);

 //内置indicator初始化
 ultraViewPager.initIndicator();
 //设置indicator样式

    ultraViewPager.getIndicator().setFocusResId(0).setNormalResId(0);
    ultraViewPager.getIndicator().setFocusColor(Color.GREEN.getValue()).setNormalColor(Color.WHITE.getValue())
                 .setRadius(10);
    if(ultraViewPager.getIndicator()!=null){
             ultraViewPager.getIndicator().setGravity(LayoutAlignment.HORIZONTAL_CENTER|LayoutAlignment.TOP);
       }

     ultraViewPager.getIndicator().build();
 //设定页面循环播放
 ultraViewPager.setInfiniteLoop(true);
 //设定页面自动切换  间隔2秒
 ultraViewPager.setAutoScroll(2000);
```

## 目前仍未实现的功能(待后续补全)

 * 一屏展示多个子条目
 * 切换的时候执行一些切换特效

## 开源许可证

UltraViewPager遵循MIT开源许可证协议。


