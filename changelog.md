### 组件目前已经实现了，指示器的方向设置
``` java
ultraViewPager.initIndicator();
ultraViewPager.getIndicator().setOrientation(UltraViewPager.Orientation.HORIZONTAL);
```
  

### 设置指示器的圆点显示，颜色和大小
``` java
 ultraViewPager.getIndicator().setFocusResId(0).setNormalResId(0);
 ultraViewPager.getIndicator().setFocusColor(Color.GREEN.getValue()).setNormalColor(Color.WHITE.getValue())
 .setRadius(10);
```
### 设置指示器的图片显示方式
``` java
ultraViewPager.getIndicator().setFocusResId(ResourceTable.Media_tm_biz_lifemaster_indicator_selected)
.setNormalResId(ResourceTable.Media_tm_biz_lifemaster_indicator_normal);
```

### 设置指示器显示位置
``` java
 ultraViewPager.getIndicator().setGravity(LayoutAlignment.HORIZONTAL_CENTER|LayoutAlignment.TOP);
```
 

### 实现了pageslider的横向和竖向滑动
* 通过setScrollMode实现方向变换
``` java
 ultraViewPager.setScrollMode(UltraViewPager.ScrollMode.HORIZONTAL);
```

### 实现了pageSlider的无限循环和切换时间控制
* setInfiniteLoop（）// 控制无限循环
* setAutoScroll（）// 控制切换的时间间隔


### 目前未实现
```
由于pageslider中缺少setPageMargin和setPageTransformer方法
组件UltraViewPager缺少一屏展示多个和切换时的变换特效
```
