/*
 *
 *  MIT License
 *
 *  Copyright (c) 2017 Alibaba Group
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 */

package com.tmall.ultraviewpager;


import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;


/**
 * 滑动切换适配
 */
public class UltraPagerAdapter extends PageSliderProvider {
    private final Context context;
    private boolean isMultiScr;

    public UltraPagerAdapter(Context context, boolean isMultiScr) {
        this.context=context;
        this.isMultiScr = isMultiScr;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        DirectionalLayout rootLayout = (DirectionalLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_layout_item_pageslider, null,false);

        Text textView = (Text) rootLayout.findComponentById(ResourceTable.Id_pager_textview);
        textView.setText(position + "");

        ShapeElement background = new ShapeElement();
        rootLayout.setId(rootLayout.getId());
        switch (position) {
            case 0:
                background.setRgbColor(new RgbColor(33,150,243));
                rootLayout.setBackground(background);
                break;
            case 1:
                background.setRgbColor(new RgbColor(103,58,183));
                rootLayout.setBackground(background);
                break;
            case 2:
                background.setRgbColor(new RgbColor(0,150,136));
                rootLayout.setBackground(background);
                break;
            case 3:
                background.setRgbColor(new RgbColor(96,125,139));
                rootLayout.setBackground(background);
                break;
            case 4:
                background.setRgbColor(new RgbColor(244,67,54));
                rootLayout.setBackground(background);
                break;
        }
        componentContainer.addComponent(rootLayout);

        return rootLayout;
    }


    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i1, Object o1) {
        DirectionalLayout view = (DirectionalLayout) o1;

        componentContainer.removeComponent(view);

    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o1) {
        return component==o1;
    }
}
