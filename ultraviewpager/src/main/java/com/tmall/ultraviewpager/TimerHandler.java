/*
 *
 *  MIT License
 *
 *  Copyright (c) 2017 Alibaba Group
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 */

package com.tmall.ultraviewpager;


import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.utils.PlainIntArray;


/**
 * 线程切换时间控制类
 */
public class TimerHandler extends EventHandler {

    /**
     * 计时监听该回调
     */
    public interface TimerHandlerListener {
        int getNextItem();
        void callBack();
    }

    PlainIntArray specialInterval;
    long interval;
    boolean isStopped = true;
    TimerHandlerListener listener;

    static final int MSG_TIMER_ID = 87108;

    /**
     *  构造方法
     *
     * @param listener 时间监听
     * @param interval 间隔毫秒值
     */
    public TimerHandler(TimerHandlerListener listener, long interval) {
        super(EventRunner.getMainEventRunner());
        this.listener = listener;
        this.interval = interval;
    }

    @Override
    protected void processEvent(InnerEvent event) {
        if(MSG_TIMER_ID==event.eventId){
            if (listener != null) {
                int nextIndex = listener.getNextItem();
                listener.callBack();
                tick(nextIndex);
            }

        }

    }


    /**
     * 间隔时间处理
     *
     * @param index 倒计时时间间隔
     */
    public void tick(int index) {
        sendEvent(TimerHandler.MSG_TIMER_ID, getNextInterval(index));
    }

    private long getNextInterval(int index) {
        long next = interval;
        if (specialInterval != null) {
            long has = specialInterval.get(index, -1);
            if (has > 0) {
                next = has;
            }
        }
        return next;
    }

    /**
     * 条件判断
     *
     * @return 是否停止
     */
    public boolean isStopped() {
        return isStopped;
    }

    /**
     * 设置是否停止
     *
     * @param stopped 是否停止
     */
    public void setStopped(boolean stopped) {
        isStopped = stopped;
    }

    /**
     * 设置监听回调
     *
     * @param listener 监听回调
     */
    public void setListener(TimerHandlerListener listener) {
        this.listener = listener;
    }

    /**
     * 设置间隔时间
     *
     * @param specialInterval 间隔时间
     */
    public void setSpecialInterval(PlainIntArray specialInterval) {
        this.specialInterval = specialInterval;
    }
}
